stm32f429_disc1_gmp
===================

Example showing how to use GNU GMP (GMP is a free library for arbitrary precision arithmetic).

I have modified the HAL TIM demo with a few calls to GMP API to show basic usage.
This will be nice to have for bignum math on STM32F4.

Note: In order to build GMP for STM32F4, see my snippets for details.

https://bitbucket.org/snippets/pymaximus/ynzEq

Place libgmp.a in top level directory, and gmp.h in Inc/

Example using HAL to handle an interrupt and toggle LED on STM32F429I-DISC1 board.

This is mainly used to test the conversion of existing STM32Cube examples
to **cmake**. 

ST Notes
--------

```
This example shows how to configure the TIM peripheral to generate a time base of 
one second with the corresponding Interrupt request.

In this example TIM3 input clock (TIM3CLK) is set to 2 * APB1 clock (PCLK1), 
since APB1 prescaler is different from 1.   
      TIM3CLK = 2 * PCLK1  
      PCLK1 = HCLK / 4 
      => TIM3CLK = HCLK / 2 = SystemCoreClock /2 (Hz)
To get TIM3 counter clock at 10 KHz, the Prescaler is computed as following:
   - Prescaler = (TIM3CLK / TIM3 counter clock) - 1

SystemCoreClock is set to 180MHz for STM32F4xx Devices.

The TIM3 ARR register value is equal to 10000 - 1, 
Update rate = TIM3 counter clock / (Period + 1) = 1 Hz,
So the TIM3 generates an interrupt each 1 s

When the counter value reaches the auto-reload register value, the TIM upadate 
interrupt is generated and, in the handler routine, PG.13 is toggled with the 
following frequency: 

- PG.13: 0.5Hz 

```

Build
-----


Run cmake

```
15:06 $ cmake -DSTM32_CHIP=STM32F429ZI -DCMAKE_TOOLCHAIN_FILE=/Users/frank/other_repos/stm32-cmake/cmake/gcc_stm32.cmake -DCMAKE_BUILD_TYPE=Debug ../
-- No TARGET_TRIPLET specified, using default: arm-none-eabi
-- No STM32_FAMILY specified, trying to get it from STM32_CHIP
-- Selected STM32 family: F4
-- The C compiler identification is GNU 6.3.1
-- The CXX compiler identification is GNU 6.3.1
-- Check for working C compiler: /Users/frank/opt/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc
-- Check for working C compiler: /Users/frank/opt/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /Users/frank/opt/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-g++
-- Check for working CXX compiler: /Users/frank/opt/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-g++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- The ASM compiler identification is GNU
-- Found assembler: /Users/frank/opt/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc
-- STM32F429ZI is 429xx device
-- Found CMSIS: /Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include;/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/CMSIS/Include  
-- Found STM32HAL: /Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Inc  
-- No linker script specified, generating default
-- STM32F429ZI has 2048KiB of flash memory and 192KiB of RAM
-- Configuring done
-- Generating done
-- Build files have been written to: /Users/frank/repos/stm32f429_disc1_gmp/build

```

Compile

```
make
Scanning dependencies of target stm32f4_example_project
[  5%] Building C object CMakeFiles/stm32f4_example_project.dir/Src/main.c.obj
[ 10%] Building C object CMakeFiles/stm32f4_example_project.dir/Src/stm32f4xx_hal_msp.c.obj
[ 15%] Building C object CMakeFiles/stm32f4_example_project.dir/Src/stm32f4xx_it.c.obj
[ 20%] Building C object CMakeFiles/stm32f4_example_project.dir/Drivers/BSP/STM32F429I-Discovery/stm32f429i_discovery.c.obj
[ 25%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/system_stm32f4xx.c.obj
[ 30%] Building ASM object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc/startup_stm32f429xx.s.obj
[ 35%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c.obj
[ 40%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c.obj
[ 45%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c.obj
[ 50%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c.obj
[ 55%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c.obj
[ 60%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c.obj
[ 65%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.c.obj
[ 70%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.c.obj
[ 75%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c.obj
[ 80%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c.obj
[ 85%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c.obj
[ 90%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c.obj
[ 95%] Building C object CMakeFiles/stm32f4_example_project.dir/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.16.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c.obj
[100%] Linking C executable stm32f4_example_project
[100%] Built target stm32f4_example_project


```

Awesome !! Now lets load using gdb via openocd 

In one terminal start openocd

```
16:04 $ openocd -f ~/stm32f4_info/stm32f4discovery.cfg
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
	http://openocd.org/doc/doxygen/bugs.html
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
adapter speed: 2000 kHz
adapter_nsrst_delay: 100
none separate
srst_only separate srst_nogate srst_open_drain connect_deassert_srst
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : clock speed 1800 kHz
Info : STLINK v2 JTAG v25 API v2 SWIM v14 VID 0x0483 PID 0x374B
Info : using stlink api v2
Info : Target voltage: 2.859872
Info : stm32f4x.cpu: hardware has 6 breakpoints, 4 watchpoints

```


The in another window run gdb to load, but first go up one level
so we find **.gdbinit**

```
15:16 $ cd ..
✔ ~/repos/stm32f429_disc1_gmp [master L|…50] 
15:16 $ 
✔ ~/repos/stm32f429_disc1_gmp [master L|…50] 
15:16 $ 
✔ ~/repos/stm32f429_disc1_gmp [master L|…50] 
15:16 $ ~/opt/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gdb build/stm32f4_example_project
GNU gdb (GNU Tools for ARM Embedded Processors 6-2017-q1-update) 7.12.1.20170215-git
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=x86_64-apple-darwin10 --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from build/stm32f4_example_project...done.
0x0801bbaa in main () at /Users/frank/repos/stm32f429_disc1_gmp/Src/main.c:157
157	  if(HAL_TIM_Base_Start_IT(&TimHandle) != HAL_OK)
semihosting is enabled
Unable to match requested speed 2000 kHz, using 1800 kHz
Unable to match requested speed 2000 kHz, using 1800 kHz
adapter speed: 1800 kHz
target halted due to debug-request, current mode: Thread 
xPSR: 0x01000000 pc: 0x0801bd40 msp: 0x2002fffc, semihosting
Loading section .isr_vector, size 0x1ac lma 0x8000000
Loading section .text, size 0x20ff8 lma 0x80001c0
Loading section .rodata, size 0x1ae0 lma 0x80211b8
Loading section .ARM, size 0x8 lma 0x8022c98
Loading section .init_array, size 0x8 lma 0x8022ca0
Loading section .fini_array, size 0x4 lma 0x8022ca8
Loading section .data, size 0x9c8 lma 0x8022cac
Start address 0x801bd40, load size 144992
Transfer rate: 24 KB/sec, 7631 bytes/write.
Unable to match requested speed 2000 kHz, using 1800 kHz
Unable to match requested speed 2000 kHz, using 1800 kHz
adapter speed: 1800 kHz
target halted due to debug-request, current mode: Thread 
xPSR: 0x01000000 pc: 0x0801bd40 msp: 0x2002fffc, semihosting
Unable to match requested speed 8000 kHz, using 4000 kHz
Unable to match requested speed 8000 kHz, using 4000 kHz
adapter speed: 4000 kHz
(gdb) c
Continuing.

```

openocd should display the following.

```
Hello GMP Test
Testing GMP library cross compiled for ARM
n = 11
19!  =  121645100408832000
42!  =  1405006117752879898543142606244511569936384000000000
```


Result
------

At this point you should see LED on PG13 toggling every second, making a 0.5Hz flash rate, and
GMP examples being printed to openocd console.

Awesome !!
